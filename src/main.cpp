#include <memory>

#include <QApplication>
#include "CommandLineParser.h"
#include "QmlEngine.h"

int main(int argc, char** argv)
{
    auto app = std::make_shared<QApplication>(argc, argv);
    app->setOrganizationDomain("kde.org");

    auto parser = std::make_shared<CommandLineParser>(*app);
    auto qml_engine = std::make_shared<QmlEngine>(parser);

    qml_engine->initialize();

    return app->exec();
}
