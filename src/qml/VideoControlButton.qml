import QtQuick 2.7
import QtQuick.Controls 2.2

import org.kde.kirigami 2.0 as Kirigami

ToolButton {
    id: control

    implicitWidth: Kirigami.Units.iconSizes.medium
    implicitHeight: implicitWidth

    property color color: control.pressed || control.checked ? Kirigami.Theme.highlightColor : Kirigami.Theme.buttonBackgroundColor
    property real iconSize: Kirigami.Units.iconSizes.smallMedium
    property string iconSource;

    contentItem: Item {
        Kirigami.Icon {
            anchors.centerIn: parent;
            width: Kirigami.Units.iconSizes.smallMedium;
            height: width
            source: control.iconSource
        }
    }
}
