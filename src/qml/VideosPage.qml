import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

import QtGraphicalEffects 1.0

import Qt.labs.settings 1.0

import org.kde.kirigami 2.4 as Kirigami
import org.kde.contentlist 0.1
import org.kde.hologram 1.0 as Hologram

Kirigami.Page {
    id: base

    title: "All Videos"

    background: BlurredBackground { }

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    property string __searchString;

    Menu {
        id: menu

        modal: true

        property QtObject openedFor
        property string filePath;

        onClosed: {
            openedFor = null;
            filePath = "";
        }

        MenuItem { text: "Play"; icon.name: "media-playback-start"; visible: menu.openedFor == null; onTriggered: base.enqueueAndPlay(menu.filePath) }
        MenuItem { text: "Add to Playlist"; icon.name: "list-add"; onTriggered: Hologram.Playlist.addItem(menu.filePath) }
        MenuItem { text: "Replace Playlist"; icon.name: "list-add"; onTriggered: Hologram.Playlist.replace(menu.filePath) }
    }

    ScrollView {
        anchors.fill: parent
        GridView {
            id: view

            property int minimumCellWidth: Kirigami.Units.gridUnit * 16
            cellWidth: {
                var contentWidth = view.width - Kirigami.Units.largeSpacing
                var columnCount = Math.floor(contentWidth / minimumCellWidth);
                return contentWidth / columnCount;
            }
            cellHeight: cellWidth * (9.0/16.0)

            header: RowLayout {
                height: searchField.height + Kirigami.Units.smallSpacing;
                width: view.width

                Item { Layout.preferredWidth: 1 }

                TextField {
                    id: searchField;

                    Layout.fillWidth: true

                    placeholderText: "Search..."
                    onTextChanged: base.__searchString = text
                }

                BusyIndicator {
                    Layout.preferredWidth: contentList.searching ? implicitWidth : 0
                    Behavior on Layout.preferredWidth { NumberAnimation { duration: Kirigami.Units.longDuration }}

                    opacity: contentList.searching ? 1 : 0;
                    Behavior on opacity { OpacityAnimator { duration: Kirigami.Units.longDuration } }
                }

                Label {
                    id: searchLabel
                    Layout.preferredWidth: contentList.searching ? implicitWidth : 0
                    Behavior on Layout.preferredWidth { NumberAnimation { duration: Kirigami.Units.longDuration }}
                    opacity: contentList.searching ? 1 : 0
                    Behavior on opacity { OpacityAnimator { duration: Kirigami.Units.longDuration } }

                    text: "Searching..."
                }
                Item { Layout.preferredWidth: contentList.searching ? Kirigami.Units.largeSpacing * 2 : Kirigami.Units.smallSpacing }
            }

            model: ContentList {
                id: contentList

                autoSearch: true
                cacheResults: true

                sortMode: ContentList.SortByName
                sortDirection: ContentList.SortAscending

                property bool searching: false

                ContentQuery {
                    type: ContentQuery.Video
                    searchString: base.__searchString
                }

                onSearchStarted: searching = true;
                onSearchCompleted: searching = false;
            }

            delegate: Loader {
                width: GridView.view.cellWidth
                height: GridView.view.cellHeight

                sourceComponent: Kirigami.AbstractCard {
                    anchors.fill: parent
                    anchors.margins: Kirigami.Units.largeSpacing

                    checkable: true
                    showClickFeedback: true

                    Image {
                        anchors.fill: parent
//                         anchors.margins: parent.checked ? Kirigami.Units.smallSpacing : 0

                        source: "image://preview/" + String(model.filePath).slice(7)
                        fillMode: Image.PreserveAspectCrop

                        sourceSize.width: width
                        sourceSize.height: height
                    }

                    Kirigami.Heading {
                        width: parent.width
                        padding: Kirigami.Units.largeSpacing

                        level: 2
                        text: model.filename

                        background: Rectangle {
                            anchors.fill: parent
                            gradient: Gradient {
                                GradientStop { position: 0.0; color: palette.dark }
                                GradientStop { position: 1.0; color: Qt.rgba(palette.dark.r, palette.dark.g, palette.dark.b, 0) }
                            }
                        }
                    }

                    Button {
                        id: playButton

                        anchors {
                            left: parent.left
                            bottom: parent.bottom
                            margins: Kirigami.Units.largeSpacing
                        }
                        text: "Play"
                        icon.name: "media-playback-start"

                        opacity: parent.hovered || parent.checked ? 1 : 0
                        Behavior on opacity { OpacityAnimator { duration: 100 } }

                        onClicked: base.enqueueAndPlay(model.filePath)
                    }

                    ToolButton {
                        anchors {
                            right: parent.right
                            verticalCenter: playButton.verticalCenter
                            margins: Kirigami.Units.largeSpacing
                        }

                        icon.name: "overflow-menu"

                        opacity: parent.hovered || parent.checked || checked ? 1 : 0
                        Behavior on opacity { OpacityAnimator { duration: 100 } }

                        checkable: true
                        checked: menu.openedFor == this;
                        onClicked: {
                            menu.openedFor = this;
                            menu.filePath = model.filePath;
                            menu.popup(this, 0, height)
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.RightButton
                        onClicked: {
                            menu.filePath = model.filePath;
                            menu.popup()
                        }
                    }
                }
            }

            footer: Item { width: Kirigami.Units.largeSpacing; height: width }
        }
    }

    Label {
        anchors.centerIn: parent;
        width: parent.width / 4;
        opacity: !contentList.searching && view.count == 0 ? 1 : 0;
        Behavior on opacity { OpacityAnimator { duration: Kirigami.Units.shortDuration } }
        text: "No results found."
        horizontalAlignment: Text.AlignHCenter
    }

    actions.main: Kirigami.Action {
        text: "Open File"
        iconSource: "document-open"
        onTriggered: openFileDialog.open()
    }

    actions.contextualActions: Kirigami.Settings.isMobile ? __sortActions : __nestedAction

    function enqueueAndPlay(file) {
        var index = Hologram.Playlist.addItem(file)
        Hologram.Playlist.currentIndex = index;
        Hologram.Video.play()
    }

    ActionGroup { id: sortGroup }
    ActionGroup { id: directionGroup }

    Settings {
        category: "VideosPage"
        property alias sortMode: contentList.sortMode
        property alias sortDirection: contentList.sortDirection
    }

    FileDialog {
        id: openFileDialog

        selectMultiple: true

        onAccepted: {
            var index = 0;
            for(var i in fileUrls) {
                index = Hologram.Playlist.addItem(fileUrls[i])
            }
            Hologram.Playlist.currentIndex = index;
            Hologram.Video.play();
        }
    }

    property list<QtObject> __sortActions: [
        Action {
            id: sortName

            text: "Name"
            checkable: true
            checked: contentList.sortMode == ContentList.SortByName
            property bool visible: true

            onTriggered: contentList.setSortMode(ContentList.SortByName)

            ActionGroup.group: sortGroup
        },
        Action {
            id: sortDate

            text: "Date"
            checkable: true
            checked: contentList.sortMode == ContentList.SortByDate
            property bool visible: true

            onTriggered: contentList.sortMode = ContentList.SortByDate

            ActionGroup.group: sortGroup
        },
        Action {
            id: sortSize

            text: "Size"
            checkable: true
            checked: contentList.sortMode == ContentList.SortBySize
            property bool visible: true

            onTriggered: contentList.sortMode = ContentList.SortBySize

            ActionGroup.group: sortGroup
        },

        Kirigami.Action { separator: true },

        Action {
            id: sortAscending

            text: "Ascending"
            checkable: true
            checked: contentList.sortDirection == ContentList.SortAscending
            property bool visible: true

            onTriggered: contentList.sortDirection = ContentList.SortAscending

            ActionGroup.group: directionGroup
        },
        Action {
            id: sortDescending

            text: "Descending"
            checkable: true
            checked: contentList.sortDirection == ContentList.SortDescending
            property bool visible: true

            onTriggered: contentList.sortDirection = ContentList.SortDescending

            ActionGroup.group: directionGroup
        }
    ]

    property Kirigami.Action __nestedAction: Kirigami.Action {
        text: "Sort: %1".arg(sortGroup.checkedAction.text)
        children: __sortActions
    }
}
