import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.2

import QtMultimedia 5.8
import Qt.labs.settings 1.0

import org.kde.kirigami 2.4 as Kirigami
import org.kde.hologram 1.0 as Hologram

Kirigami.Page {
    id: page

    title: "Playlist"

    background: BlurredBackground { }

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    ScrollView {
        anchors.fill: parent
        Kirigami.CardsListView {
            model: Hologram.Playlist

            delegate: Kirigami.AbstractCard {
                id: card
                height: Kirigami.Units.gridUnit * 4

                showClickFeedback: true
                checkable: true

                Image {
                    id: thumbnail
                    height: parent.height
                    width: (16/9) * height

                    source: "image://preview/" + model.filePath
                    fillMode: Image.PreserveAspectCrop

                    sourceSize.width: width
                    sourceSize.height: height
                }

                Kirigami.Icon {
                    id: nowPlayingIcon

                    anchors.left: thumbnail.right
                    anchors.leftMargin: Kirigami.Units.largeSpacing
                    anchors.bottom: fileNameLabel.top

                    width: Kirigami.Units.iconSizes.small
                    height: Kirigami.Units.iconSizes.small

                    opacity: model.playing ? 0.6 : 0
                    Behavior on opacity { OpacityAnimator { duration: Kirigami.Units.shortDuration } }

                    source: "media-playback-start"
                }

                Label {
                    anchors.left: nowPlayingIcon.right
                    anchors.leftMargin: Kirigami.Units.smallSpacing
                    anchors.verticalCenter: nowPlayingIcon.verticalCenter

                    opacity: nowPlayingIcon.opacity
                    text: "Now Playing:"
                }

                Kirigami.Heading {
                    id: fileNameLabel
                    anchors.left: thumbnail.right
                    anchors.leftMargin: Kirigami.Units.largeSpacing
                    anchors.verticalCenter: thumbnail.verticalCenter
                    text: model.fileName
                    level: 2
                }

                ColumnLayout {
                    anchors {
                        right: parent.right
                        top: parent.top
                        bottom: parent.bottom
                        margins: Kirigami.Units.smallSpacing
                    }

                    opacity: !model.playing && (parent.checked || parent.hovered)
                    Behavior on opacity { OpacityAnimator { duration: Kirigami.Units.shortDuration } }

                    Button {
                        icon.name: "media-playback-start"
                        text: "Play"

                        onClicked: {
                            Hologram.Playlist.currentIndex = index
                            Hologram.Video.play()
                        }
                    }

                    ToolButton {
                        Layout.alignment: Qt.AlignRight
                        icon.name: "overflow-menu"

                        checkable: true
                        checked: menu.openedFor == this;
                        onClicked: {
                            menu.openedFor = this;
                            menu.index = index
                            menu.popup(this, 0, height)
                        }
                    }
                }

                contentItem: Item { Layout.preferredHeight: Kirigami.Units.gridUnit * 2; }
            }
        }
    }

    Menu {
        id: menu

        property QtObject openedFor: null
        property int index: -1

        MenuItem {
            icon.name: "delete";
            text: "Remove";
            onTriggered: {
                Hologram.Playlist.removeItem(menu.index)
                menu.openedFor = null
            }
        }
    }

    actions.contextualActions: [
        Kirigami.Action {
            text: "Play Mode: " + playModeGroup.checkedAction.text

            Action {
                id: normal

                text: "Normal"
                checkable: true
                checked: Hologram.Playlist.playbackMode == Playlist.Sequential

                onTriggered: Hologram.Playlist.playbackMode = Playlist.Sequential

                ActionGroup.group: playModeGroup
            }
            Action {
                id: shuffle

                text: "Shuffle"
                checkable: true
                checked: Hologram.Playlist.playbackMode == Playlist.Random

                onTriggered: Hologram.Playlist.playbackMode = Playlist.Random

                ActionGroup.group: playModeGroup
            }
            Action {
                id: repeatAll

                text: "Repeat Everything"
                checkable: true
                checked: Hologram.Playlist.playbackMode == Playlist.Loop

                onTriggered: Hologram.Playlist.playbackMode = Playlist.Loop

                ActionGroup.group: playModeGroup
            }
            Action {
                id: repeatOne

                text: "Repeat Current"
                checkable: true
                checked: Hologram.Playlist.playbackMode == Playlist.CurrentItemLoop

                onTriggered: Hologram.Playlist.playbackMode = Playlist.CurrentItemLoop

                ActionGroup.group: playModeGroup
            }
        }
    ]

    ActionGroup { id: playModeGroup }

    Settings {
        category: "Playlist"
        property int playbackMode: Hologram.Playlist.playbackMode
    }
}
