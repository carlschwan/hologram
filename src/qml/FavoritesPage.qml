import QtQuick 2.9
import QtQuick.Controls 2.2
import Qt.labs.folderlistmodel 2.1

import org.kde.kirigami 2.0 as Kirigami

Kirigami.ScrollablePage {
    title: "Favorites"

    ListView {
        anchors.fill: parent
        model: FolderListModel {
            rootFolder: "/home/ahiemstra/Videos";
            folder: "/home/ahiemstra/Videos"
            showOnlyReadable: true
            showDirsFirst: true
            nameFilters: ["*.mp4", "*.mkv", "*.wmv", "*.ogv", "*.avi"]
        }

        delegate: Kirigami.BasicListItem {
            icon: "video-mp4"
            label: model.fileName
            onClicked: {
                root.video = model.filePath
                root.pageStack.replace(nowPlayingComponent)
            }
        }
    }
}

