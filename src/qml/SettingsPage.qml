import QtQuick 2.9
import QtQuick.Controls 2.2

import org.kde.kirigami 2.4 as Kirigami
import org.kde.hologram 1.0 as Hologram

Kirigami.Page {
    title: "Settings"

    background: BlurredBackground { }

    Label { anchors.centerIn: parent; text: "Settings should go here." }
}
