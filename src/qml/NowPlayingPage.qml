import QtQuick 2.9
import QtQuick.Controls 2.2

import QtMultimedia 5.8

import org.kde.kirigami 2.0 as Kirigami

import org.kde.hologram 1.0 as Hologram

Kirigami.Page {
    id: base

    title: "Now Playing: %1".arg(Hologram.Video.playlist.currentFileName)

    background: Item { }

    topPadding: 0
    bottomPadding: 0
    leftPadding: 0
    rightPadding: 0

    contextualActions: [
        Kirigami.Action {
            text: "Audio Streams..."
            onTriggered: audioStreamOverlay.open()

            enabled: Hologram.Video.audioStreams.count > 1
        },
        Kirigami.Action {
            text: "Subtitles..."
            onTriggered: subtitleOverlay.open()

            enabled: Hologram.Video.subtitles.count > 1
        }
    ]

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: controls.hidden ? Qt.BlankCursor : Qt.ArrowCursor
        onPositionChanged: controls.show()
        onClicked: Hologram.Video.togglePlayback()
        onDoubleClicked: Hologram.Video.fullScreen = !Hologram.Video.fullScreen
    }

    VideoControls {
        id: controls
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        Connections {
            target: Hologram.Video
            onFullScreenChanged: {
                if(applicationWindow().fullscreen) {
                    controls.hide()
                } else {
                    controls.show()
                }
            }
        }
    }

    Row {
        parent: applicationWindow().pageStack.globalToolBar.rightHandleAnchor.parent

        spacing: Kirigami.Units.smallSpacing

        ToolButton {
            id: muteButton

            icon.name: Hologram.Video.audio.muted ? "player-volume-muted" : "player-volume"
            checkable: true
            checked: Hologram.Video.audio.muted
            onToggled: Hologram.Video.audio.muted = checked
        }

        Slider {
            id: volumeSlider

            anchors.verticalCenter: parent.verticalCenter

            enabled: !muteButton.checked

            width: Kirigami.Units.gridUnit * 5
            value: Hologram.Video.audio.volume

            onMoved: Hologram.Video.audio.volume = value
        }

        Item {
            width: Kirigami.Settings.isMobile ? Kirigami.Units.smallSpacing : Kirigami.Units.iconSizes.medium + Kirigami.Units.smallSpacing * 2
            height: 1
        }
    }

    Shortcut {
        sequence: "Space"
        onActivated: {
            controls.focusPlayButton()
            Hologram.Video.togglePlayback()
        }
    }

    Kirigami.OverlaySheet {
        id: audioStreamOverlay

        ListView {
            implicitWidth: base.width / 2

            model: Hologram.Video.audioStreams;

            delegate: Kirigami.BasicListItem {
                height: Kirigami.Units.gridUnit * 2
                width: ListView.view.width / 2

                text: "Audio Stream %1: %2".arg(index + 1).arg(model.name)

                checkable: true
                checked: model.active

                onClicked: Hologram.Video.audioStreams.setActive(index);
            }
        }
    }

    Kirigami.OverlaySheet {
        id: subtitleOverlay

        ListView {
            implicitWidth: base.width / 2

            model: Hologram.Video.subtitles;

            delegate: Kirigami.BasicListItem {
                height: Kirigami.Units.gridUnit * 2
                width: ListView.view.width / 2

                text: index == 0 ? model.name : "Subtitle %1: %2".arg(index).arg(model.name)

                checkable: true
                checked: model.active

                onClicked: Hologram.Video.subtitles.setActive(index);
            }
        }
    }
}
