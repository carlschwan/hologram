/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CommandLineParser.h"

#include <QUrl>
#include <QApplication>
#include <QDebug>

#include <QFile>
#include <QDir>

#include <QMimeDatabase>
#include <QMultimedia>
#include <QMediaPlayer>

CommandLineParser::CommandLineParser(QApplication& qapp)
{
    setApplicationDescription(QStringLiteral("A Video Player"));
    addHelpOption();
    addVersionOption();

    addPositionalArgument(QStringLiteral("files"), QStringLiteral("A list of files to add to the playlist"), QStringLiteral("[files...]"));

    addOptions({
        { "fullscreen", "Start in fullscreen mode" },
        { "muted", "Start with audio muted" },
    });

    process(qapp);

    auto files = positionalArguments();
    for(const auto& file : files)
    {
        auto url = QUrl::fromUserInput(file);
        addRecursively(url);
    }
}

CommandLineParser::~CommandLineParser()
{
}

QList<QUrl> CommandLineParser::files() const
{
    return m_files;
}

bool CommandLineParser::startFullScreen() const
{
    return isSet(QStringLiteral("fullscreen"));
}

bool CommandLineParser::startMuted() const
{
    return isSet(QStringLiteral("muted"));
}

void CommandLineParser::addRecursively(const QUrl& url)
{
    auto fileInfo = QFileInfo{url.toLocalFile()};

    if(!fileInfo.exists()) {
        qDebug() << "Ignoring non-existing file" << url;
        return;
    }

    if(!fileInfo.isReadable()) {
        qDebug() << "Ignoring unreadable file" << url;
        return;
    }

    if(fileInfo.isDir()) {
        auto dir = QDir{url.toLocalFile()};
        for(const auto& entry : dir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries)) {
            addRecursively(QUrl::fromLocalFile(dir.absoluteFilePath(entry)));
        }

        return;
    }

    QMimeDatabase db;
    auto mime = db.mimeTypeForFile(url.toLocalFile()).name();
    auto support = QMediaPlayer::hasSupport(mime);

    if(support == QMultimedia::NotSupported) {
        // QMediaPlayer::hasSupport incorrectly reports certain video formats as unsupported.
        // So instead try to play everything starting with video/ regardless of the result.
        if(!mime.startsWith("video")) {
            return;
        }
    }

    if(mime.startsWith("image")) {

        return;
    }

    m_files.append(url);
}
