/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SubtitlesModel.h"

#include <QTimer>
#include <QMediaService>
#include <QMediaStreamsControl>
#include <QMediaMetaData>
#include "VideoProxy.h"

SubtitlesModel::SubtitlesModel(VideoProxy* parent)
    : QAbstractListModel(parent)
{
    m_parent = parent;
    connect(m_parent, &VideoProxy::mediaObjectChanged, this, &SubtitlesModel::onMediaObjectChanged);
    onMediaObjectChanged();

    connect(this, &SubtitlesModel::rowsInserted, this, &SubtitlesModel::countChanged);
    connect(this, &SubtitlesModel::rowsRemoved, this, &SubtitlesModel::countChanged);
    connect(this, &SubtitlesModel::modelReset, this, &SubtitlesModel::countChanged);
}

SubtitlesModel::~SubtitlesModel()
{
}

bool SubtitlesModel::enabled() const
{
    return m_enabled;
}

QHash<int, QByteArray> SubtitlesModel::roleNames() const
{
    static QHash<int, QByteArray> roleNames {
        {IndexRole, "streamIndex"},
        {NameRole, "name"},
        {ActiveRole, "active"},
    };
    return roleNames;
}

int SubtitlesModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
        return 0;

    return m_streams.size();
}

QVariant SubtitlesModel::data(const QModelIndex& index, int role) const
{
    if(!index.isValid())
        return QVariant{};

    switch(role)
    {
        case IndexRole:
            return m_streams.at(index.row()).index;
        case NameRole:
            return m_streams.at(index.row()).name;
        case ActiveRole:
            return m_streams.at(index.row()).active;
        default:
            return QVariant{};
    }
}

void SubtitlesModel::setEnabled(bool enabled)
{
    if(enabled == m_enabled)
        return;

    // Glorious hackery time.
    // QtMultimedia currently has no way of actually disabling a subtitle stream.
    // Subtitles are enabled through an environment variable - at least when using
    // GStreamer, so instead (ab)use that for enabling/disabling subtitles.
    //
    // Yes, this is really ugly.
    m_enabled = enabled;
    if(m_enabled)
    {
        qputenv("QT_GSTREAMER_PLAYBIN_FLAGS", "4");
    }
    else
    {
        qputenv("QT_GSTREAMER_PLAYBIN_FLAGS", "");
    }
    m_parent->recreateMediaPlayer();

    onActiveStreamsChanged();

    emit enabledChanged();
}

void SubtitlesModel::setActive(int row)
{
    if(row < 0 || row >= int(m_streams.size()))
        return;

    if(m_streams.at(row).active)
        return;

    auto newEnabled = row != 0;
    if(newEnabled != m_enabled)
    {
        setEnabled(newEnabled);
        QTimer::singleShot(75, [this, row](){ setActive(row); });
        return;
    }

    for(std::size_t i = 0; i < m_streams.size(); ++i)
    {
        m_streams.at(i).active = (int(i) == row);
    }
    emit dataChanged(index(0), index(m_streams.size() - 1), QVector<int>{ActiveRole});

    if(m_streamControl)
        m_streamControl->setActive(m_streams.at(row).index, true);
}

void SubtitlesModel::onMediaObjectChanged()
{
    beginResetModel();

    if(m_streamControl) {
        disconnect(m_streamControl, &QMediaStreamsControl::streamsChanged, this, &SubtitlesModel::onStreamsChanged);
        disconnect(m_streamControl, &QMediaStreamsControl::activeStreamsChanged, this, &SubtitlesModel::onActiveStreamsChanged);
    }

    auto mediaObject = m_parent->mediaObject();
    if(!mediaObject)
        return;

    m_streamControl = qobject_cast<QMediaStreamsControl*>(mediaObject->service()->requestControl("org.qt-project.qt.mediastreamscontrol/5.0"));
    if(m_streamControl) {
        connect(m_streamControl, &QMediaStreamsControl::streamsChanged, this, &SubtitlesModel::onStreamsChanged);
        connect(m_streamControl, &QMediaStreamsControl::activeStreamsChanged, this, &SubtitlesModel::onActiveStreamsChanged);
        onStreamsChanged();
    }

    endResetModel();
}

void SubtitlesModel::onStreamsChanged()
{
    beginResetModel();

    m_streams.clear();

    Subtitle subtitle;
    subtitle.index = -1;
    subtitle.name = "No Subtitles";
    subtitle.active = !m_enabled;
    m_streams.push_back(subtitle);

    for(int i = 0; i < m_streamControl->streamCount(); ++i)
    {
        if(m_streamControl->streamType(i) != QMediaStreamsControl::SubPictureStream)
            continue;

        Subtitle stream{};
        stream.index = i;
        stream.name = m_streamControl->metaData(i, QMediaMetaData::Title).toString();
        stream.active = m_enabled && m_streamControl->isActive(i);
        if(stream.name.isEmpty())
            stream.name = m_streamControl->metaData(i, QMediaMetaData::Language).toString();

        m_streams.push_back(stream);
    }

    endResetModel();
}

void SubtitlesModel::onActiveStreamsChanged()
{
    m_streams[0].active = !m_enabled;
    for(auto& stream : m_streams)
    {
        if(stream.index >= 0)
        {
            stream.active = m_enabled && m_streamControl->isActive(stream.index);
        }
    }

    emit dataChanged(index(0), index(m_streams.size() - 1), QVector<int>{ActiveRole});
}
