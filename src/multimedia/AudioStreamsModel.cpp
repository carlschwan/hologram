/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "AudioStreamsModel.h"

#include <QMediaService>
#include <QMediaStreamsControl>
#include <QMediaMetaData>
#include "VideoProxy.h"

AudioStreamsModel::AudioStreamsModel(VideoProxy* parent)
    : QAbstractListModel(parent)
{
    m_parent = parent;
    connect(m_parent, &VideoProxy::mediaObjectChanged, this, &AudioStreamsModel::onMediaObjectChanged);
    onMediaObjectChanged();

    connect(this, &AudioStreamsModel::rowsInserted, this, &AudioStreamsModel::countChanged);
    connect(this, &AudioStreamsModel::rowsRemoved, this, &AudioStreamsModel::countChanged);
    connect(this, &AudioStreamsModel::modelReset, this, &AudioStreamsModel::countChanged);
}

AudioStreamsModel::~AudioStreamsModel()
{
}

QHash<int, QByteArray> AudioStreamsModel::roleNames() const
{
    static QHash<int, QByteArray> roleNames {
        {IndexRole, "streamIndex"},
        {NameRole, "name"},
        {ActiveRole, "active"},
    };
    return roleNames;
}

int AudioStreamsModel::rowCount(const QModelIndex& parent) const
{
    if(parent.isValid())
        return 0;

    return m_streams.size();
}

QVariant AudioStreamsModel::data(const QModelIndex& index, int role) const
{
    if(!index.isValid())
        return QVariant{};

    switch(role)
    {
        case IndexRole:
            return m_streams.at(index.row()).index;
        case NameRole:
            return m_streams.at(index.row()).name;
        case ActiveRole:
            return m_streams.at(index.row()).active;
        default:
            return QVariant{};
    }
}

void AudioStreamsModel::setActive(int row)
{
    if(row == m_activeStream)
        return;

    if(row < 0 || row >= int(m_streams.size()))
        return;

    m_activeStream = row;

    for(std::size_t i = 0; i < m_streams.size(); ++i)
    {
        m_streams.at(i).active = (int(i) == row);
    }
    emit dataChanged(index(0), index(m_streams.size() - 1), QVector<int>{ActiveRole});

    if(m_streamControl)
        m_streamControl->setActive(m_streams.at(row).index, true);
}

void AudioStreamsModel::onMediaObjectChanged()
{
    beginResetModel();

    if(m_streamControl) {
        disconnect(m_streamControl, &QMediaStreamsControl::streamsChanged, this, &AudioStreamsModel::onStreamsChanged);
        disconnect(m_streamControl, &QMediaStreamsControl::activeStreamsChanged, this, &AudioStreamsModel::onActiveStreamsChanged);
    }

    auto mediaObject = m_parent->mediaObject();
    if(!mediaObject)
        return;

    m_streamControl = qobject_cast<QMediaStreamsControl*>(mediaObject->service()->requestControl("org.qt-project.qt.mediastreamscontrol/5.0"));
    if(m_streamControl) {
        connect(m_streamControl, &QMediaStreamsControl::streamsChanged, this, &AudioStreamsModel::onStreamsChanged);
        connect(m_streamControl, &QMediaStreamsControl::activeStreamsChanged, this, &AudioStreamsModel::onActiveStreamsChanged);
        onStreamsChanged();
    }

    endResetModel();
}

void AudioStreamsModel::onStreamsChanged()
{
    beginResetModel();
    auto oldStreams = m_streams;
    m_streams.clear();

    for(int i = 0; i < m_streamControl->streamCount(); ++i)
    {
        if(m_streamControl->streamType(i) != QMediaStreamsControl::AudioStream)
            continue;

        AudioStream stream{};
        stream.index = i;
        stream.name = m_streamControl->metaData(i, QMediaMetaData::Language).toString();
        stream.active = m_streamControl->isActive(i);
        m_streams.push_back(stream);
    }

    if(oldStreams.size() == m_streams.size())
    {
        for(std::size_t i = 0; i < oldStreams.size(); ++i)
        {
            auto& stream = m_streams.at(i);
            stream.active = oldStreams.at(i).active;
            if(m_streamControl)
                m_streamControl->setActive(stream.index, stream.active);
        }
    }

    endResetModel();
}

void AudioStreamsModel::onActiveStreamsChanged()
{
    for(int i = 0; i < m_streamControl->streamCount(); ++i)
    {
        if(m_streamControl->streamType(i) != QMediaStreamsControl::AudioStream)
            continue;

        m_streams[i].active = m_streamControl->isActive(i);
    }

    emit dataChanged(index(0), index(m_streams.size() - 1), QVector<int>{ActiveRole});
}
