/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYLISTPROXY_H
#define PLAYLISTPROXY_H

#include <QMediaPlaylist>
#include <QAbstractListModel>

class VideoProxy;
class CommandLineParser;

/**
 * @todo write docs
 */
class PlaylistModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(QString currentFileName READ currentFileName NOTIFY currentIndexChanged)
    Q_PROPERTY(bool hasNext READ hasNext NOTIFY hasNextChanged)
    Q_PROPERTY(bool hasPrevious READ hasPrevious NOTIFY hasPreviousChanged)
    Q_PROPERTY(bool hasItems READ hasItems NOTIFY itemsChanged)
    Q_PROPERTY(int playbackMode READ playbackMode WRITE setPlaybackMode NOTIFY playbackModeChanged)

public:
    enum Roles
    {
        FileNameRole = Qt::UserRole + 1,
        FilePathRole,
        FileUrlRole,
        PlayingRole,
    };

    /**
     * Default constructor
     */
    PlaylistModel(VideoProxy* parent, const CommandLineParser& parser);

    /**
     * Destructor
     */
    ~PlaylistModel();

    QMediaPlaylist* mediaPlaylist() const;

    int rowCount(const QModelIndex & parent) const override;
    QVariant data(const QModelIndex & index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    bool hasNext() const;
    bool hasPrevious() const;
    bool hasItems() const;

    int currentIndex() const;
    QString currentFileName() const;

    int playbackMode() const;

public Q_SLOTS:
    int addItem(const QUrl& source);
    void removeItem(int index);

    void setCurrentIndex(int index);

    void setPlaybackMode(int newMode);

    void next();
    void previous();

Q_SIGNALS:
    void itemsChanged();
    void hasNextChanged();
    void hasPreviousChanged();
    void currentIndexChanged();
    void playbackModeChanged();

private:
    VideoProxy* m_parent = nullptr;
    QMediaPlaylist* m_playlist;
};

#endif // PLAYLISTPROXY_H
