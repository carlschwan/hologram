/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2018  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VIDEOPROXY_H
#define VIDEOPROXY_H

#include <memory>
#include <QObject>
#include <QMediaPlayer>

class PlaylistModel;
class AudioStreamsModel;
class SubtitlesModel;
class AudioGroupedProperty;
class CommandLineParser;

/**
 * @todo write docs
 */
class VideoProxy : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QMediaPlayer* mediaObject READ mediaObject NOTIFY mediaObjectChanged)

    Q_PROPERTY(PlaylistModel* playlist READ playlist CONSTANT)
    Q_PROPERTY(AudioGroupedProperty* audio READ audio CONSTANT)
    Q_PROPERTY(AudioStreamsModel* audioStreams READ audioStreams CONSTANT)
    Q_PROPERTY(SubtitlesModel* subtitles READ subtitles CONSTANT)

    Q_PROPERTY(bool playing READ isPlaying NOTIFY playingChanged)
    Q_PROPERTY(bool paused READ isPaused NOTIFY pausedChanged)
    Q_PROPERTY(bool stopped READ isStopped NOTIFY stoppedChanged)
    Q_PROPERTY(float progress READ progress NOTIFY progressChanged)

    Q_PROPERTY(bool fullScreen READ fullScreen WRITE setFullScreen NOTIFY fullScreenChanged)

    Q_PROPERTY(QString currentTime READ currentTime NOTIFY currentTimeChanged)
    Q_PROPERTY(QString totalTime READ totalTime NOTIFY totalTimeChanged)
public:
    /**
     * Default constructor
     */
    VideoProxy(const CommandLineParser& parser, QObject* parent = nullptr);

    /**
     * Destructor
     */
    ~VideoProxy();

    QMediaPlayer* mediaObject() const;

    PlaylistModel* playlist() const;
    AudioGroupedProperty* audio() const;
    AudioStreamsModel* audioStreams() const;
    SubtitlesModel* subtitles() const;

    QString name() const;

    bool isPlaying() const;
    bool isPaused() const;
    bool isStopped() const;
    float progress() const;

    bool fullScreen() const;

    QString currentTime() const;
    QString totalTime() const;

    void recreateMediaPlayer();

public Q_SLOTS:
    void setFullScreen(bool full);

    void play();
    void pause();
    void stop();

    void togglePlayback();

    void seek(float position);

Q_SIGNALS:
    void mediaObjectChanged();

    void playingChanged();
    void pausedChanged();
    void stoppedChanged();
    void progressChanged();

    void fullScreenChanged();

    void currentTimeChanged();
    void totalTimeChanged();

    void startPlayback();

private:
    class Private;
    const std::unique_ptr<Private> d;

};

#endif // VIDEOPROXY_H
